package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    private var firstPlayer = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        buttonR.setOnClickListener() {
            restgame()

        }
        button00.setOnClickListener() {
            checkPlayer(button00)
        }
        button01.setOnClickListener() {
            checkPlayer(button01)
        }
        button02.setOnClickListener() {
            checkPlayer(button02)
        }


        button10.setOnClickListener() {
            checkPlayer(button10)
        }
        button11.setOnClickListener() {
            checkPlayer(button11)
        }
        button12.setOnClickListener() {
            checkPlayer(button12)
        }
        button20.setOnClickListener() {
            checkPlayer(button20)
        }
        button21.setOnClickListener() {
            checkPlayer(button21)
        }
        button22.setOnClickListener() {
            checkPlayer(button22)
        }
    }

    private fun restgame() {

        button00.text = ""
        button10.text = ""
        button11.text = ""
        button12.text = ""
        button20.text = ""
        button21.text = ""
        button22.text = ""
        button01.text = ""
        button02.text = ""
    }


    private fun checkPlayer(button: Button) {
        if (button.text.isEmpty()) {
            if (firstPlayer) {
                button.text = "x"
                checkWinner()
                firstPlayer = false
            } else {
                button.text = "0"
                checkWinner()
                firstPlayer = true
            }

        }

    }

    private fun checkWinner() {

        if (button00.text == "x" && button01.text == "x" && button02.text == "x") {
            Toast.makeText(this, "Winner x", Toast.LENGTH_LONG).show()
            restgame()
        } else if (button00.text == "0" && button01.text == "0" && button02.text == "0") {
            Toast.makeText(this, "Winner 0", Toast.LENGTH_LONG).show()
            restgame()
        } else if (button10.text == "x" && button11.text == "x" && button12.text == "x") {
            Toast.makeText(this, "Winner x", Toast.LENGTH_LONG).show()
            restgame()
        } else if (button10.text == "0" && button11.text == "0" && button12.text == "0") {
            Toast.makeText(this, "winner 0", Toast.LENGTH_LONG).show()
            restgame()
        } else if (button20.text == "0" && button21.text == "0" && button22.text == "0") {
            Toast.makeText(this, "winner 0", Toast.LENGTH_LONG).show()
            restgame()
        } else if (button20.text == "x" && button21.text == "x" && button22.text == "x") {
            Toast.makeText(this, "winner x", Toast.LENGTH_LONG).show()
            restgame()
        } else if (button00.text == "x" && button10.text == "x" && button20.text == "x") {
            Toast.makeText(this, "winner x", Toast.LENGTH_LONG).show()
            restgame()
        } else if (button00.text == "0" && button10.text == "0" && button20.text == "0") {
            Toast.makeText(this, "winner 0", Toast.LENGTH_LONG).show()
            restgame()
        } else if (button01.text == "x" && button11.text == "x" && button21.text == "x") {
            Toast.makeText(this, "Winner x ", Toast.LENGTH_LONG).show()
            restgame()
        } else if (button01.text == "0" && button11.text == "0" && button21.text == "0") {
            Toast.makeText(this, "winner 0", Toast.LENGTH_LONG).show()
            restgame()
        } else if (button02.text == "0" && button12.text == "0" && button22.text == "0") {
            Toast.makeText(this, "winner 0", Toast.LENGTH_LONG).show()
            restgame()
        } else if (button02.text == "x" && button12.text == "x" && button22.text == "x") {
            Toast.makeText(this, "winner x", Toast.LENGTH_LONG).show()
            restgame()
        } else if (button00.text == "x" && button11.text == "x" && button22.text == "x") {
            Toast.makeText(this, "winner x", Toast.LENGTH_LONG).show()
            restgame()
        } else if (button00.text == "0" && button11.text == "0" && button22.text == "0") {
            Toast.makeText(this, "winner 0", Toast.LENGTH_LONG).show()
            restgame()
        } else if (button20.text == "0" && button11.text == "0" && button02.text == "0") {
            Toast.makeText(this, "winner 0", Toast.LENGTH_LONG).show()
            restgame()
        } else if (button20.text == "x" && button11.text == "x" && button02.text == "x") {
            Toast.makeText(this, "winner x", Toast.LENGTH_LONG).show()
            restgame()

        }

        if (button00.text.isNotEmpty() && button01.text.isNotEmpty() && button02.text.isNotEmpty() && button10.text.isNotEmpty() && button11.text.isNotEmpty() && button12.text.isNotEmpty() && button20.text.isNotEmpty() && button21.text.isNotEmpty() && button22.text.isNotEmpty()) {
            Toast.makeText(this, "მეგობრობამ გაიმარჯვა....თამაში დასრულდა ფრედ", Toast.LENGTH_LONG).show()

        }


    }


}
